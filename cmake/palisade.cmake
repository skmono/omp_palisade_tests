# ******************************************************************************
# Copyright 2020 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
# ******************************************************************************

include(ExternalProject)

set(PALISADE_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/ext_palisade)
set(PALISADE_SRC_DIR ${PALISADE_PREFIX}/src/ext_palisade/)
set(PALISADE_REPO_URL https://gitlab.com/palisade/palisade-release.git)
set(PALISADE_GIT_TAG master)

# TODO: use lower/variable bit length?
ExternalProject_Add(
  ext_palisade
  GIT_REPOSITORY ${PALISADE_REPO_URL}
  GIT_TAG ${PALISADE_GIT_TAG}
  PREFIX ${PALISADE_PREFIX}
  CMAKE_ARGS
    -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX}
    -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
    -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
    -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}
    -DCMAKE_INSTALL_INCLUDEDIR=${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}
    -DBigIntegerBitLength=600
    -DBUILD_UNITTESTS=OFF
    -DBUILD_EXAMPLES=OFF
    -DBUILD_BENCHMARKS=ON
    -DBUILD_EXTRAS=OFF
    -DWITH_BE2=ON
    -DWITH_BE4=OFF
    -DWITH_TCM=OFF
    -DUSE_OpenMP=OFF
    -DWITH_NATIVEOPT=ON
    -DWITH_INTEL_LATTICE=${ENABLE_INTEL_LATTICE}

  BUILD_COMMAND $(MAKE) -j all
  # Skip updates
  UPDATE_COMMAND ""
)

ExternalProject_Get_Property(ext_palisade SOURCE_DIR BINARY_DIR)

# PALISADE core
add_library(libpalisade_core INTERFACE)
add_dependencies(libpalisade_core ext_palisade)
target_include_directories(libpalisade_core SYSTEM
                           INTERFACE ${PALISADE_SRC_DIR}/src/core/include)
target_include_directories(libpalisade_core SYSTEM
                           INTERFACE ${BINARY_DIR}/src/core)
target_include_directories(libpalisade_core SYSTEM
                           INTERFACE ${SOURCE_DIR}/third-party/cereal/include)
target_link_libraries(libpalisade_core
                      INTERFACE ${BINARY_DIR}/lib/libPALISADEcore.so)

# PALISADE binfhe
add_library(libpalisade_binfhe INTERFACE)
add_dependencies(libpalisade_binfhe ext_palisade)
target_include_directories(libpalisade_binfhe SYSTEM
                           INTERFACE ${PALISADE_SRC_DIR}/src/binfhe/include)
target_include_directories(libpalisade_binfhe SYSTEM
                           INTERFACE ${SOURCE_DIR}/third-party/cereal/include)
target_link_libraries(libpalisade_binfhe
                      INTERFACE ${BINARY_DIR}/lib/libPALISADEbinfhe.so)

# PALISADE pke
add_library(libpalisade_pke INTERFACE)
add_dependencies(libpalisade_pke ext_palisade libpalisade_core)

target_include_directories(libpalisade_pke SYSTEM
                           INTERFACE ${PALISADE_SRC_DIR}/src/pke/include)
target_link_libraries(libpalisade_pke INTERFACE libpalisade_core)
target_link_libraries(libpalisade_pke
                      INTERFACE ${BINARY_DIR}/lib/libPALISADEpke.so)

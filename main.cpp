#include <omp.h>
#include <thread>
#include <cstdint>


#include "utils.hpp"
int run()
{
    // Generate BFV context
    int poly_modulus_degree = 8192;
    std::vector<int> coeff_modulus_bits(3);
    int plaintext_modulus = 65537;
    lbcrypto::SecurityLevel security_level = lbcrypto::HEStd_NotSet;
    float dist = 3.19;
    int num_adds = 0;
    int num_mults = coeff_modulus_bits.size();
    int num_key_switches = 0;
    MODE mode = OPTIMIZED;
    int max_depth = 2;

    uint64_t relin_window = 0;
    int dcrt_bits = 60;

    auto cc = lbcrypto::CryptoContextFactory<lbcrypto::DCRTPoly>::
        genCryptoContextBFVrns(plaintext_modulus, security_level, dist, num_adds,
                               num_mults, num_key_switches, mode, max_depth, relin_window,
                               dcrt_bits, poly_modulus_degree);

    cc->Enable(PKESchemeFeature::ENCRYPTION);
    cc->Enable(PKESchemeFeature::SHE);


    // Prepare data
    int dim1=10, dim2=9;

    int64_t slots = 8192;
    int64_t spacers = slots / dim2;

    int n =0;
    std::vector<int32_t> vec(dim2);
    std::generate(std::begin(vec), std::end(vec), [&n, &spacers] { return n+=spacers; });
    
    auto keys = cc->KeyGen();
    cc->EvalMultKeyGen(keys.secretKey);
    cc->EvalAtIndexKeyGen(keys.secretKey, vec);
    cc->EvalSumKeyGen(keys.secretKey);

    std::vector<int64_t> data(slots, 7);

    std::vector<lbcrypto::Ciphertext<lbcrypto::DCRTPoly>> vec_cipher_a(dim1);
    for(size_t i=0; i<dim1;i++){
        lbcrypto::Plaintext plain = cc->MakePackedPlaintext(data);
        vec_cipher_a[i] = cc->Encrypt(keys.publicKey, plain);
    }

    lbcrypto::Plaintext plain = cc->MakePackedPlaintext(data);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> cipher_b = cc->Encrypt(keys.publicKey, plain);
    plain.reset();

    // Run MatMulRow threaded
    const int old_max_active_levels = omp_get_max_active_levels();
    const int old_nested_value = omp_get_nested();
    omp_set_nested(true);
    omp_set_max_active_levels(2);

    std::vector<lbcrypto::Ciphertext<lbcrypto::DCRTPoly>> sum(dim1);
    lbcrypto::Plaintext plain_zero = cc->MakePackedPlaintext(std::vector<int64_t>{0});
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> cipher_zero = cc->Encrypt(keys.publicKey, plain_zero);
    plain_zero.reset();

    int inner_threads = omp_get_max_threads() / vec_cipher_a.size() - 1;
    if(inner_threads<=0)
        inner_threads = 1;
    else if (inner_threads > vec_cipher_a.size())
        inner_threads = vec_cipher_a.size();
    
    int omp_remaining_threads = omp_get_max_threads();
    std::cout<<"Inner threads: "<<inner_threads<<", remaining_threads: "<<omp_remaining_threads<<std::endl;
#pragma omp parallel for num_threads(omputils::assignOMPThreads(omp_remaining_threads, vec_cipher_a.size()))
    for(size_t i=0; i<vec_cipher_a.size();i++){
        lbcrypto::Ciphertext<lbcrypto::DCRTPoly> cipher_res_tmp = cc->EvalMult(vec_cipher_a[i], cipher_b);
        lbcrypto::Ciphertext<lbcrypto::DCRTPoly> tmp_sum = cipher_zero;
#pragma omp declare reduction(+: \
                            lbcrypto::Ciphertext<lbcrypto::DCRTPoly>: \
                            omp_out += omp_in) \
                            initializer(omp_priv = omp_orig)
#pragma omp parallel for reduction(+: tmp_sum) num_threads(\
    omputils::assignOMPThreads(omp_remaining_threads, inner_threads))
        for(int32_t k=1; k<dim2; k++){
            tmp_sum += cc->EvalAtIndex(cipher_res_tmp, k * spacers);
        }
        sum[i] = tmp_sum + cipher_res_tmp;
    }
    omp_set_max_active_levels(old_max_active_levels);
    omp_set_nested(old_nested_value);

    return 0;
}

int main()
{
    for(int i=0; i<100; ++i){
        std::cout<<"Trial: "<<i<<" -- ";
        run();
    }

}

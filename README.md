OpenMP Palisade BFV MatMul tests

# How to build
```
mkdir build
cd build
cmake ..
make
```

Run with ```OMP_NUM_THREADS=$(nproc) ./test```

#include <iostream>
#include <vector>
#include <palisade.h>

template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  out << "{";
  size_t last = v.size() - 1;
  for (size_t i = 0; i < v.size(); ++i) {
    out << v[i];
    if (i != last) out << ", ";
  }
  out << "}";
  return out;
}

class omputils {
 public:
    static int assignOMPThreads(int& remaining_threads, int requested_threads) {
      int retval = (requested_threads > 0 ? requested_threads : 1);
      if (retval > remaining_threads) retval = remaining_threads;
      if (retval > 1)
          remaining_threads -= retval;
      else
          retval = 1;
      return retval;
    }
};
